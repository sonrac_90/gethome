<?php

/**
 * Class UserIdentity
 */
class UserIdentity extends CUserIdentity
{


    protected $_id;

    private function getUser($name) {

        $criteria = new CDbCriteria;
        $criteria->addCondition('LOWER(email) = :email', 'OR');
        $criteria->addCondition('LOWER(phone) = :phone', 'OR');
        $criteria->params = array(
            ':email' => $name,
            ':phone' => $name,
        );

        return Users::model()->find($criteria);
    }

    // Данный метод вызывается один раз при аутентификации пользователя.
    public function authenticate()
    {
        /** @var $user Users **/
        // Производим стандартную аутентификацию, описанную в руководстве.
        $user = $this->getUser($this->username);

        if(!$user || is_null($user) || $user->password != md5($this->password))
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        else
        {
            // В качестве идентификатора будем использовать id, а не username,
            // как это определено по умолчанию. Обязательно нужно переопределить
            // метод getId(см. ниже).
            $this->_id = $user->id;

            // Далее логин нам не понадобится, зато имя может пригодится
            // в самом приложении. Используется как Yii::app()->user->name.
            // realName есть в нашей модели. У вас это может быть name, firstName
            // или что-либо ещё.
            $this->username  = $user->userinfo0->first_name . " " . $user->userinfo0->last_name;
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}