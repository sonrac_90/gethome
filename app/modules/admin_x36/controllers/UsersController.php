<?php
class UsersController extends BaseAdminController {


    public function actionIndex () {
        /** @var Users $users */
        $users = new Users('search');
        $users->unsetAttributes();

        if (isset($_POST['Users'])) {
            $users->attributes = $_POST['Users'];
        }

        $this->render('index', array(
            'users' => $users
        ));
    }

    public function actionUpdate($id) {
        $criteria = new CDbCriteria;
        $criteria->with = array(
            'soc_user',
            'userinfo0',
        );
        $criteria->together = true;
        $criteria->addCondition('t.id = :id');
        $criteria->params = array(
            ':id' => $id
        );
        /** @var Users $user */
        $user = Users::model()->find($criteria);

        if (isset($_POST['Users'])) {
            $user->attributes = $_POST['Users'];

            if ($user->update() && $user->validate()) {
                if (isset($_POST['UserProfile'])) {
                    $user->userinfo0->attributes = $_POST['UserProfile'];

                    $user->userinfo0->birthday = ($user->userinfo0->birthday) ? : null;

                    if ($user->userinfo0->validate() && $user->userinfo0->save()) {
                        $this->redirect(GPath::getURL($this, 'users/index'));
                    }
                }
            }
        }

        $this->render(
            '_form', array(
                'user' => $user
            )
        );
    }

}