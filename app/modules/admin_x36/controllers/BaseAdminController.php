<?php

class BaseAdminController extends Controller {

    public $layout = 'main';

    public $title = '';

    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            array(
                'allow',
                'actions' => array(
                    'login',
                    'logout',
                    'error'
                ),
                'users'   => array('*'),
            ),
            array(
                'allow',
                'roles' => array('user'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionLogin() {
        $this->layout = '/layouts/login';
        $this->title  = 'Авторизация';
        $model        = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()
               ->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        $this->layout = '/layouts/error';
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }

}