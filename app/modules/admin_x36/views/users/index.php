

<?php

/** @var Users $users */

$this->widget(
    'zii.widgets.grid.CGridView', array(
        'id'               => 'users-list',
        'dataProvider'     => $users->search(),
        'ajaxType'         => 'POST',
        'filter'           => $users,
        'enablePagination' => true,
        'itemsCssClass'    => 'table table-bordered table-striped dataTable',
        'template'         => '{items}{pager}',
        //            'filterCssClass'   => 'form-control',
        'pager'            => array(
            'header'               => '',
            'prevPageLabel'        => '<',
            'nextPageLabel'        => '>',
            'lastPageLabel'        => '>>',
            'firstPageLabel'       => '<<',
            'hiddenPageCssClass'   => '',
            'pageSize'             => 50,
            'htmlOptions'          => array('class' => 'pagination pagination-centered'),
            'selectedPageCssClass' => 'active',
        ),
        'pagerCssClass'    => 'p',
        'columns'          => array(
            'id',
            'email',
            array(
                'name' => 'last_name',
                'value' => '$data->userinfo0->last_name'
            ),
            array(
                'name' => 'first_name',
                'value' => '$data->userinfo0->first_name'
            ),
            array(
                'name' => 'city',
                'value' => '$data->userinfo0->city'
            ),
            'phone',
            array(
                'name' => 'role',
                'value' => function ($data) {
                    /** @var Users $data */
                    return (($data->role == 'admin') ? 'Администратор' : 'Пользователь');
                },
                'filter' => array(
                    'admin' => 'Администратор',
                    'user'  => 'Пользователь',
                ),
            ),
            array(
                'name' => 'soc_name',
                'value' => function ($data) {
                    $soc = SocUser::model()->getSocName();
                    return $soc[$data->soc_name->soc_name];
                },
                'filter' => SocUser::model()->getSocName(),
            ),
            array(
                'name' => 'soc_id',
                'value' => function ($data) {
                    return $data->soc_name->soc_id;
                },
                'filter' => false
            ),
            array(
                'class'       => 'CButtonColumn',
                'htmlOptions' => array(
                    'style' => 'width: 43px'
                ),
                'template'    => '<div class="buttonTPL">{update}{delete}</div>',
                'buttons'     => array(
                    'update' => array(
                        'title' => 'Редактировать',
                        'imageUrl' => false,
                        'label' => '',
                        'options'=>array('class'=>'glyphicon glyphicon-edit'),
                    ),
                    'delete' => array(
                        'title' => 'Удалить',
                        'imageUrl' => false,
                        'label' => '',
                        'options'=>array('class'=>'glyphicon glyphicon-remove'),
                        'visible' => '$data->role!="admin"',
                    ),
                ),
            )
        )
    )
);