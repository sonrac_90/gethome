
<?php
/* @var $this UsersController */
/* @var $user Users */
/* @var $userInfo UserProfile */
/* @var $form CActiveForm */

$userInfo = $user->userinfo0;

/*






 */

GCScript::regScript(array(
        array(
            'name' => $this->module->assetsUrl . '/plugins/clockface/css/clockface.css',
            'pos'  => CClientScript::POS_BEGIN,
        ),
        array(
            'name' => $this->module->assetsUrl . '/plugins/bootstrap-datepicker/css/datepicker.css',
            'pos'  => CClientScript::POS_BEGIN,
        ),
        array(
            'name' => $this->module->assetsUrl . '/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
            'pos'  => CClientScript::POS_BEGIN,
        ),
        array(
            'name' => $this->module->assetsUrl . '/plugins/bootstrap-colorpicker/css/colorpicker.css',
            'pos'  => CClientScript::POS_BEGIN,
        ),
        array(
            'name' => $this->module->assetsUrl . '/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
            'pos'  => CClientScript::POS_BEGIN,
        ),
        array(
            'name' => $this->module->assetsUrl . '/plugins/bootstrap-datetimepicker/css/datetimepicker.css',
            'pos'  => CClientScript::POS_BEGIN,
        ),
        $this->module->assetsUrl . '/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
        $this->module->assetsUrl . '/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        $this->module->assetsUrl . '/plugins/clockface/js/clockface.js',
        $this->module->assetsUrl . '/plugins/bootstrap-daterangepicker/moment.min.js',
        $this->module->assetsUrl . '/plugins/bootstrap-daterangepicker/daterangepicker.js',
        $this->module->assetsUrl . '/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
        $this->module->assetsUrl . '/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        $this->module->assetsUrl . '/scripts/custom/components-pickers.js',
    ),
    CClientScript::POS_END);

?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <?php echo (($user->id) ? 'Редактирование пользователя #' . $user->id : 'Добавление пользователя'); ?>
        </div>
    </div>

    <div class="portlet-body form">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'users-_form-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation'=>false,
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>

        <div class="form-body">
            <blockquote>
                <p >Поля, отмеченные <span class="required">*</span> Обязательны для заполнения.</p>
            </blockquote>
            <?php echo $form->errorSummary($user); ?>

            <div class="form-group">
                <?php echo $form->labelEx($userInfo,'last_name',array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9">
                    <?php echo $form->textField($userInfo,'last_name', array('class' => 'form-control')); ?>
                    <?php echo $form->error($userInfo,'last_name', array('class' => 'help-block')); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($userInfo,'first_name',array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9">
                    <?php echo $form->textField($userInfo,'first_name', array('class' => 'form-control')); ?>
                    <?php echo $form->error($userInfo,'first_name', array('class' => 'help-block')); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($userInfo,'birthday',array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <?php echo $form->textField($userInfo,'birthday', array('class' => 'form-control', 'readonly' => 'readonly')); ?>
<!--                        <input type="text" class="form-control" readonly >-->
                        <span class="input-group-btn">
                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                    <?php echo $form->error($userInfo,'birthday', array('class' => 'help-block')); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($user,'phone',array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9">
                    <?php $this->widget(
                        'CMaskedTextField', array(
                            'model' => $user,
                            'attribute' => 'phone',
                            'mask' => '+ 38 (999) 999 - 99 - 99',
                            'htmlOptions' => array('class' => 'form-control')
                        )
                    ); ?>
                    <?php echo $form->error($user,'phone', array('class' => 'help-block')); ?>
                </div>
            </div>
        </div>
        <div class="form-actions center-block" align="center">
            <?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')); ?>
        </div>
    </div>
</div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function () {
        ComponentsPickers.init();
    })
</script>