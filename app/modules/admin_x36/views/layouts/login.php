<?php

$assetsUrl = $this->module->assetsUrl;
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $this->title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <?php
    GCScript::regScript(
        array(
            'jquery',
            array(
                'name' => 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
                'type' => 'css'
            ),
            $assetsUrl . '/plugins/font-awesome/css/font-awesome.min.css',
            $assetsUrl . '/plugins/bootstrap/css/bootstrap.min.css',
            $assetsUrl . '/plugins/bootstrap/css/bootstrap.min.css',
            $assetsUrl . '/plugins/uniform/css/uniform.default.css',
            $assetsUrl . '/plugins/select2/select2.css',
            $assetsUrl . '/plugins/select2/select2-metronic.css',
            $assetsUrl . '/css/style-metronic.css',
            $assetsUrl . '/css/style.css',
            $assetsUrl . '/css/plugins.css',
            $assetsUrl . '/css/themes/default.css',
            $assetsUrl . '/css/pages/error.css',
            $assetsUrl . '/css/pages/login-soft.css',
            $assetsUrl . '/css/custom.css',
            'js: baseUrl = "' . $assetsUrl . '";',
            //     <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
            array(
                'name' => $assetsUrl . '/plugins/jquery-migrate-1.2.1.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/bootstrap/js/bootstrap.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/jquery.blockui.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/jquery.cokie.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/uniform/jquery.uniform.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/jquery-validation/dist/jquery.validate.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/backstretch/jquery.backstretch.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/select2/select2.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/scripts/core/app.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/scripts/custom/login-soft.js',
                'pos'  => CClientScript::POS_END
            ),
        )
    );
    ?>

    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="<?php echo GPath::getURL(null, '/'); ?>">
		<img src="<?php echo $assetsUrl; ?>/img/logo-big.png" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">

   <?php echo $content; ?>

</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 2014 &copy; GetHome - Административная панель.
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
	<script src="assets/plugins/excanvas.min.js"></script>
<![endif]-->

    <script>
		jQuery(document).ready(function () {
            App.init();
            Login.init();
        });
	</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>