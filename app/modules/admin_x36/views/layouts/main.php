<?php

$assetsUrl = $this->module->assetsUrl;
?>
<!DOCTYPE html>

<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $this->title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>

    <?php
    GCScript::regScript(array(
                            'jquery',
                            ///*
                            // <!-- BEGIN GLOBAL MANDATORY STYLES -->
                            array(
                                'name' => 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
                                'type' => 'css'
                            ),
                            $assetsUrl . '/plugins/font-awesome/css/font-awesome.min.css',
                            $assetsUrl . '/plugins/bootstrap/css/bootstrap.min.css',
                            $assetsUrl . '/plugins/uniform/css/uniform.default.css',
                            $assetsUrl . '/plugins/gritter/css/jquery.gritter.css',
                            // <!-- END GLOBAL MANDATORY STYLES -->


                            // <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
                            // DateRangePicker Style
                            $assetsUrl . '/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                            // Calendar style
                            $assetsUrl . '/plugins/fullcalendar/fullcalendar/fullcalendar.css',
                            // Jquery vector map style
                            $assetsUrl . '/plugins/jqvmap/jqvmap/jqvmap.css',
                            // Chart style
                            $assetsUrl . '/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css',
                            // <!-- END PAGE LEVEL PLUGIN STYLES -->


                            // <!-- BEGIN THEME STYLES -->
                            $assetsUrl . '/css/style-metronic.css',
                            $assetsUrl . '/css/style.css',
                            $assetsUrl . '/css/style-responsive.css',
                            $assetsUrl . '/css/plugins.css',
                            $assetsUrl . '/css/pages/tasks.css',
                            $assetsUrl . '/css/themes/default.css',
                            $assetsUrl . '/css/admin.css',
                            array(
                                'name' => $assetsUrl . '/css/print.css',
                                'media' => 'print'
                            ),
                            $assetsUrl . '/css/custom.css',
                            // <!-- END THEME STYLES -->
                            //*/

                            // <!-- BEGIN CORE PLUGINS -->
                            array(
                                'name' => $assetsUrl . '/plugins/jquery-migrate-1.2.1.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
//                            array(
//                                'name' => $assetsUrl . '/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js',
//                                'pos'  => CClientScript::POS_END,
//                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/bootstrap/js/bootstrap.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jquery.blockui.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jquery.cokie.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/uniform/jquery.uniform.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            // <!-- END CORE PLUGINS -->


                            // <!-- BEGIN PAGE LEVEL PLUGINS -->
                            array(
                                'name' => $assetsUrl . '/plugins/jqvmap/jqvmap/jquery.vmap.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/flot/jquery.flot.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/flot/jquery.flot.resize.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/flot/jquery.flot.categories.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jquery.pulsate.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/bootstrap-daterangepicker/moment.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/bootstrap-daterangepicker/daterangepicker.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/gritter/js/jquery.gritter.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/plugins/jquery.sparkline.min.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            // <!-- END PAGE LEVEL PLUGINS -->


                            // <!-- BEGIN PAGE LEVEL SCRIPTS -->
                            array(
                                'name' => $assetsUrl . '/scripts/core/app.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/scripts/custom/index.js',
                                'pos'  => CClientScript::POS_END,
                            ),
                            array(
                                'name' => $assetsUrl . '/scripts/custom/tasks.js',
                                'pos'  => CClientScript::POS_END,
                            )
                            // <!-- END PAGE LEVEL SCRIPTS -->


                        ), CClientScript::POS_HEAD, '');
    ?>

    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
		<a class="navbar-brand" href="index.html">
			<img src="<?php echo $assetsUrl; ?>/img/logo.png" alt="logo" class="img-responsive"/>
		</a>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<img src="<?php echo $assetsUrl; ?>/img/menu-toggler.png" alt=""/>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->


        <?php
        // Notifications area
        $this->renderPartial('/layouts/navigation');
        ?>


	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

    <?php
    // Menu sidebar
    $this->renderPartial('/layouts/right-sidebar');
    ?>

    <!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
            <?php echo $content; ?>
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">
		 2014 &copy; Metronic by keenthemes.
	</div>
	<div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->

<script>
jQuery(document).ready(function () {
    App.init(); // initlayout and core plugins
    Index.init();
    Index.initJQVMAP(); // init index page's custom scripts
    Index.initCalendar(); // init index page's custom scripts
    Index.initCharts(); // init index page's custom scripts
    Index.initChat();
    Index.initMiniCharts();
    Index.initDashboardDaterange();
    Index.initIntro();
    Tasks.initDashboardWidget();
});
</script>


<!--[if lt IE 9]>
<script src="<?php echo $assetsUrl; ?>/plugins/respond.min.js"></script>
<script src="<?php echo $assetsUrl; ?>/plugins/excanvas.min.js"></script>
<![endif]-->


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>