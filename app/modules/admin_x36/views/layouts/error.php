<?php

$assetsUrl = $this->module->assetsUrl;
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Metronic | Extra - 404 Page Option 3</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <?php

    GCScript::regScript(
        array(
            'jquery',
            array(
                'name' => 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
                'type' => 'css'
            ),
            $assetsUrl . '/plugins/font-awesome/css/font-awesome.min.css',
            $assetsUrl . '/plugins/bootstrap/css/bootstrap.min.css',
            $assetsUrl . '/plugins/bootstrap/css/bootstrap.min.css',
            $assetsUrl . '/plugins/uniform/css/uniform.default.css',
            $assetsUrl . '/css/style-metronic.css',
            $assetsUrl . '/css/plugins.css',
            $assetsUrl . '/css/themes/default.css',
            $assetsUrl . '/css/pages/error.css',
            $assetsUrl . '/css/custom.css',
            //     <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
            array(
                'name' => $assetsUrl . '/plugins/respond.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/excanvas.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/jquery-migrate-1.2.1.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/bootstrap/js/bootstrap.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/jquery.blockui.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/jquery.cokie.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/plugins/uniform/jquery.uniform.min.js',
                'pos'  => CClientScript::POS_END
            ),
            array(
                'name' => $assetsUrl . '/scripts/core/app.js',
                'pos'  => CClientScript::POS_END
            ),
        )
    );

    ?>
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<?php echo $content; ?>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $assetsUrl;?>/plugins/respond.min.js"></script>
<script src="<?php echo $assetsUrl;?>/plugins/excanvas.min.js"></script>
<![endif]-->
<script>
jQuery(document).ready(function () {
    App.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>