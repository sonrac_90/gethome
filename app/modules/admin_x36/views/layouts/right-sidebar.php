<?php
function getLI($icon, $link, $component, $text, $controller) {
    $linkAdd = $link;
    if (!$link) {
        $linkAdd = 'default/index';
    }

    return '<li class="start ' . (GPath::activeLink($linkAdd) ? 'active' : '') . '">' .
           '<a href="' . (GPath::getURL($controller, $linkAdd)) . '">' .
           '<i class="fa ' . $icon . '"></i>' .
           '<span class="title">' .
           $text .
           '</span>' .
           '<span class="' . (GPath::activeLink($linkAdd) ? 'selected' : '') . '">' .
           '</span>' .
           '</a>' .
           '</li>';
}

?>

<!-- BEGIN SIDEBAR -->

<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
                <?php
                echo getLI('fa-home', '', '/default/', 'Главная', $this);
                echo getLI('fa-users', '/users/', 'users/index', 'Пользователи', $this);
                echo getLI('fa-th-list', '/ad/', 'ad/index', 'Объявления', $this);
                echo getLI('fa-hand-o-right', '/company/', 'company/index', 'Компании', $this);
                echo getLI('fa-caret-square-o-up', '/consulting/', 'consulting/index', 'Консультанты', $this);
                echo getLI('fa-thumbs-down', '/banlist/', 'banList/index', 'Бан-лист', $this);
                echo getLI('fa-hand-o-up', '/feedback/', 'feedback/index', 'Обратная связь', $this);
                echo getLI('fa-wheelchair', '/claim/', 'claim/index', 'Жалобы', $this);
                ?>
            </ul>
        </div>
    </div>
<!-- END SIDEBAR -->