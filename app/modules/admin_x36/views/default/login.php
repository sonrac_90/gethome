<!-- BEGIN LOGIN FORM -->
<form class="login-form" action="" method="post" novalidate="novalidate">
    <h3 class="form-title">Войдите для продолжения</h3>
    <?php if ($model->errors) { ?>
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span>
                 Введите правильно email адиминистратора (или телефон) и пароль
            </span>
        </div>
    <?php } ?>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Имя администратора, email или телефон</label>
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Пользователь"
                   name="LoginForm[username]">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Пароль</label>
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Пароль"
                   name="LoginForm[password]">
        </div>
    </div>
    <div class="form-actions">
        <label class="checkbox">
        <div class="checker"><span><input type="checkbox" name="remember"
                                          value="1"></span></div> Запомнить меня </label>
        <button type="submit" class="btn blue pull-right">
        Войти <i class="m-icon-swapright m-icon-white"></i>
        </button>
    </div>

<!--    <div class="forget-password">-->
<!--        <h4>Forgot your password ?</h4>-->
<!--        <p>-->
<!--             no worries, click-->
<!--            <a href="javascript:;" id="forget-password">-->
<!--                 here-->
<!--            </a>-->
<!--             to reset your password.-->
<!--        </p>-->
<!--    </div>-->
</form>
<!-- END LOGIN FORM -->
<!-- BEGIN FORGOT PASSWORD FORM -->
<!--<form class="forget-form" action="index.html" method="post" novalidate="novalidate">-->
<!--    <h3>Forget Password ?</h3>-->
<!--    <p>-->
<!--         Enter your e-mail address below to reset your password.-->
<!--    </p>-->
<!--    <div class="form-group">-->
<!--        <div class="input-icon">-->
<!--            <i class="fa fa-envelope"></i>-->
<!--            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email">-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="form-actions">-->
<!--        <button type="button" id="back-btn" class="btn">-->
<!--        <i class="m-icon-swapleft"></i> Back </button>-->
<!--        <button type="submit" class="btn blue pull-right">-->
<!--        Submit <i class="m-icon-swapright m-icon-white"></i>-->
<!--        </button>-->
<!--    </div>-->
<!--</form>-->
<!-- END FORGOT PASSWORD FORM -->