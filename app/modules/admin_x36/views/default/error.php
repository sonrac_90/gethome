<body class="page-404-3">
<div class="page-inner">
	<img src="assets/img/pages/earth.jpg" class="img-responsive" alt="">
</div>
<div class="container error-404">
    <h1><?php echo $code; ?></h1>
	<h2>Возникли проблемы</h2>
	<p>
        <div class="error">
            <?php echo CHtml::encode($message); ?>
        </div>
    </p>
    <p>
		<a href="<?php echo GPath::getURL($this, '/default/index'); ?>">
			 Вернуться на главную
		</a>
		<br>
	</p>


</div>