<?php

class Admin_x36Module extends CWebModule {
    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(array(
                             $this->getId() . '.models.*',
                             $this->getId() . '.components.*',
                             $this->getId() . '.controllers.*',
                         ));

        Yii::app()
           ->setComponents(
               array(
                   'errorHandler' => array(
                       'errorAction' => $this->getId() . '/default/error',
                   ),
                   'user'         => array(
                       'class'    => 'WebUser',
                       'loginUrl' => Yii::app()
                                        ->createUrl($this->getId() . '/default/login'),
                   ),
                   'session'      => array(
                       'class'       => 'CHttpSession',
                       'sessionName' => 'ADMINPHPSESSID',
                   ),
               )
           );

        Yii::app()->user->setStateKeyPrefix('_admin_x36');
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // if userindentity as user - redirect to main page site
            if (!Yii::app()->user->isGuest) {
                if (!Yii::app()->user->checkAccess('admin')) {
                    //В случае, если пользователь авторизировался с правами user, перенаправляем его на основную страницу
                    $controller->redirect(Yii::app()
                                             ->createUrl('/'));
                }
            }
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else {
            return false;
        }
    }

    public function getAssetsUrl() {
        return GPath::adminUrl();
    }
}
