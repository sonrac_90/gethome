<?php

Yii::setPathOfAlias('shared', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'shared');
Yii::setPathOfAlias('yiiTemp', Yii::getPathOfAlias('shared'));

function getBaseUrl () {
    $dir = dirname($_SERVER['SCRIPT_NAME']);
    $dir = ($dir == '/' || $dir == '\\') ? '' : $dir;

    return $dir;
}

return array(
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'       => PROJECT_NAME,

    'preload'    => array('log'),

    'import'     => array(
        'application.models.*',
        'application.components.*',
        'application.helpers.*',
        'application.helpers.GFile.*',
    ),

    'modules'    => array(
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'p',
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
        'admin_x36' => array(

        ),
    ),

    'components' => array(
        'assetManager' => array(
            'basePath' => Yii::getPathOfAlias('yiiTemp') . '/assets',
            'baseUrl'  => getBaseUrl() . '/shared/assets'
        ),

        'clientScript'=>array(
            'scriptMap'=>array(
                'jquery.js'=>'//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
            ),
        ),

        'user'         => array(
            'class'          => 'WebUser',
            'allowAutoLogin' => true,
        ),
        'authManager'  => array(
            'class'        => 'PhpAuthManager',
            'defaultRoles' => array('guest'),
        ),

        'urlManager'   => array(
            'urlFormat'      => 'path',
            'showScriptName' => false,
            'caseSensitive'  => false,
            'rules'          => array(
                '<controller:\w+>/<id:\d+>'              => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
            ),
        ),

        'db'=>array(
            'connectionString' => 'pgsql:host=' . HOST . ';port=' . PORT . ';dbname=' . DB_NAME,
            'emulatePrepare' => true,
            'username' => USER,
            'password' => PASS,
            'charset' => CHARSET,
            'tablePrefix' => DB_PREFIX,
        ),

        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log'          => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),

    'params'     => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
    ),
    'language' => 'ru',
    'sourceLanguage' => 'ru',
);