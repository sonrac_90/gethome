<?php

/**
 * This is the model class for table "{{soc_user}}".
 *
 * The followings are the available columns in table '{{soc_user}}':
 *
 * @property string $id
 * @property string $soc_name
 * @property string $soc_id
 * @property string $first_name
 * @property string $last_name
 * @property string $birthday
 * @property string $city
 * @property string $phone
 */
class SocUser extends CActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return SocUser the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{soc_user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'id, soc_name, soc_id',
                'required'
            ),
            array(
                'soc_name, first_name, last_name, birthday, city',
                'length',
                'max' => 255
            ),
            array(
                'phone',
                'safe'
            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'id, soc_name, soc_id, first_name, last_name, birthday, city, phone',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'users'       => array(
                self::HAS_ONE,
                'User',
                'soc_user'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'         => 'ID',
            'soc_name'   => 'Имя соцсети',
            'soc_id'     => 'Соц ID',
            'first_name' => 'Имя',
            'last_name'  => 'Фамилия',
            'birthday'   => 'Дата рождения',
            'city'       => 'Город',
            'phone'      => 'Телефон',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('soc_name', $this->soc_name, true);
        $criteria->compare('soc_id', $this->soc_id, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('birthday', $this->birthday, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('phone', $this->phone, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getSocName () {
        return array(
            ''   => 'С сайта',
            'OK' => 'Одноклассники',
            'FB' => 'FaceBook',
            'GG' => 'Google',
            'VK' => 'ВКонтакте',
        );
    }
}
