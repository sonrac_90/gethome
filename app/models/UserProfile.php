<?php

/**
 * This is the model class for table "{{user_profile}}".
 *
 * The followings are the available columns in table '{{user_profile}}':
 *
 * @property integer $id
 * @property string  $first_name
 * @property string  $last_name
 * @property string  $middle_name
 * @property string  $birthday
 * @property string  $date_replace
 * @property string  $replace_user_id
 * @property string  $city
 *
 * The followings are the available model relations:
 * @property Users[]  $users
 * @property Users    $replaceUser
 */
class UserProfile extends CActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return UserProfile the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{user_profile}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'first_name, last_name',
                'required'
            ),
            array(
                'first_name, last_name, middle_name, city',
                'length',
                'max' => 255
            ),
            array(
                'birthday',
                'date',
                'format' => 'YYYY-mm-dd',
            ),
            array(
                'date_replace, replace_user_id',
                'safe'
            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'id, first_name, last_name, birthday, city, middle_name, date_replace, replace_user_id',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'users'       => array(
                self::HAS_ONE,
                'Users',
                'userinfo'
            ),
            'replaceUser' => array(
                self::BELONGS_TO,
                'Users',
                'replace_user_id'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'              => 'ID',
            'first_name'      => 'Фамилия',
            'last_name'       => 'Имя',
            'middle_name'     => 'Отчество',
            'city'            => 'Город',
            'birthday'        => 'Дата рождения',
            'date_replace'    => 'Дата последнего заполнения',
            'replace_user_id' => 'Replace User',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('middle_name', $this->middle_name, true);
        $criteria->compare('birthday', $this->birthday, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('date_replace', $this->date_replace, true);
        $criteria->compare('replace_user_id', $this->replace_user_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
