<?php

/**
 * This is the model class for table "{{company_info}}".
 *
 * The followings are the available columns in table '{{company_info}}':
 *
 * @property integer $id
 * @property string  $company_name
 * @property string  $city
 * @property string  $region
 * @property string  $address
 * @property string  $description
 * @property string  $short_description
 * @property string  $date_register
 * @property string  $user_created
 * @property string  $phone
 *
 * The followings are the available model relations:
 * @property User    $userCreated
 */
class CompanyInfo extends CActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return CompanyInfo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{company_info}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'company_name, city, region, address, description, short_description, date_register, user_created, phone',
                'required'
            ),
            array(
                'company_name, city, region',
                'length',
                'max' => 255
            ),
            array(
                'address, short_description',
                'length',
                'max' => 1500
            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'id, company_name, city, region, address, description, short_description, date_register, user_created, phone',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'userCreated' => array(
                self::BELONGS_TO,
                'User',
                'user_created'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'                => 'ID',
            'company_name'      => 'Название',
            'city'              => 'Город',
            'region'            => 'Регион',
            'address'           => 'Адресс',
            'description'       => 'Описание',
            'short_description' => 'Короткое описание',
            'date_register'     => 'Дата регистрации',
            'user_created'      => 'Пользователь',
            'phone'             => 'Телефоны',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('company_name', $this->company_name, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('region', $this->region, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('short_description', $this->short_description, true);
        $criteria->compare('date_register', $this->date_register, true);
        $criteria->compare('user_created', $this->user_created, true);
        $criteria->compare('phone', $this->phone, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
