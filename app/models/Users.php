<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 *
 * @property string        $id
 * @property string        $phone
 * @property string        $email
 * @property string        $password
 * @property string        $date_register
 * @property string        $date_last_visit
 * @property integer       $userinfo
 * @property integer       $companyinfo
 *
 * The followings are the available model relations:
 * @property UserProfile   $userinfo0
 * @property UserProfile[] $userProfiles
 * @property CompanyInfo[] $companyInfos
 * @property string $role
 */
class Users extends CActiveRecord {

    public $first_name;
    public $last_name;
    public $city;
    public $soc_name;
    public $soc_id;

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'phone, email, password, date_register, date_last_visit',
                'required'
            ),
            array(
                'id',
                'numerical',
                'integerOnly' => true,
            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'id, phone, email, role, first_name, city, last_name, role, password, date_register, date_last_visit, userinfo, companyinfo',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'userinfo0'    => array(
                self::BELONGS_TO,
                'UserProfile',
                'userinfo'
            ),
            'soc_user'    => array(
                self::BELONGS_TO,
                'SocUser',
                'soc_user'
            ),
            'userProfiles' => array(
                self::HAS_MANY,
                'UserProfile',
                'replace_user_id'
            ),
            'companyInfos' => array(
                self::HAS_MANY,
                'CompanyInfo',
                'user_created'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'              => 'ID',
            'phone'           => 'Телефон',
            'email'           => 'Email',
            'first_name'      => 'Имя',
            'last_name'       => 'Фамилия',
            'city'            => 'Город',
            'password'        => 'Пароль',
            'date_register'   => 'Дата регистрации',
            'date_last_visit' => 'Дата последней активности',
            'userinfo'        => 'Профиль пользователя',
            'companyinfo'     => 'Профиль компании',
            'role'            => 'Роль',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = array(
            'userinfo0'
        );

        $criteria->together = true;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.role', $this->role);
        $criteria->compare('t.phone', $this->phone, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.password', $this->password, true);
        $criteria->compare('t.date_register', $this->date_register, true);
        $criteria->compare('t.date_last_visit', $this->date_last_visit, true);
        $criteria->compare('userinfo0.last_name', $this->last_name, true);
        $criteria->compare('userinfo0.first_name', $this->first_name, true);
        $criteria->compare('userinfo0.city', $this->city, true);
        $criteria->compare('soc_user.soc_name', $this->soc_name, true);
        $criteria->compare('soc_user.soc_id', $this->soc_id, true);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 't.id DESC',
                    'attributes' => array(
                        '*',
                        'last_name' => array(
                            'asc' => 'userinfo0.last_name ASC',
                            'desc' => 'userinfo0.last_name DESC',
                        ),
                        'first_name' => array(
                            'asc' => 'userinfo0.first_name ASC',
                            'desc' => 'userinfo0.first_name DESC',
                        ),
                        'city' => array(
                            'asc' => 'userinfo0.city ASC',
                            'desc' => 'userinfo0.city DESC',
                        ),
                        'soc_name' => array(
                            'asc' => 'soc_user.soc_name ASC',
                            'desc' => 'soc_user.soc_name DESC',
                        ),
                        'soc_id' => array(
                            'asc' => 'soc_user.soc_id ASC',
                            'desc' => 'soc_user.soc_id DESC',
                        ),
                    ),
                ),
            )
        );
    }
}
