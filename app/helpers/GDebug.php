<?php

class GDebug {

    public static $isDebug = false;

    public static function dump($val) {
        if (self::$isDebug) {
            echo "<pre>";
            var_dump($val);
            echo "</pre>";
        }
    }

    public static function regDebug() {
        Yii::app()->session->add('debug', true);
        self::$isDebug = true;
    }

    public static function check() {
        return Yii::app()->session['debug'] || self::$isDebug;
    }

    public static function destroy() {
        Yii::app()->session->remove('debug');
    }

}