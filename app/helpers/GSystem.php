<?php

/**
 * System info and device info
 * Class GSystem
 */
class GSystem {

    /**
     * Get server's system info
     *
     * @param null|array $params
     *
     * @return array
     */
    public static function getInfo($params = null) {
        return GSystemHelper::getInstance()
                            ->getInfo($params);
    }

    /**
     * Get name mobile device or false
     *
     * @return bool|string
     */
    public static function getMobilePlatform() {
        $android = self::isAndroid();
        $webOS   = self::isWebOS();
        $wPhone  = self::isWindowsPhone();
        $iPod    = self::isIPod();
        $iPad    = self::isIPad();
        $iPhone  = self::isIPhone();

        $mobileDevice = (
            ($android) ? $android :
                (($webOS) ? $webOS :
                    (($wPhone) ? $wPhone :
                        (($iPad) ? $iPad :
                            (($iPod) ? $iPod :
                                (($iPhone) ? $iPhone :
                                    false
                                )
                            )
                        )
                    )
                )
        );

        return $mobileDevice;
    }

    /**
     * Check is mobile device or false
     *
     * @return bool
     */
    public static function isMobile() {
        return (self::getMobilePlatform()) ? true : false;
    }

    /**
     * Check iPad or false
     *
     * @return bool|string
     */
    public static function isIPad() {
        return ((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'iPad') ? 'iPad' : false);
    }

    /**
     * Check iPhone or false
     *
     * @return bool|string
     */
    public static function isIPhone() {
        return ((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone') ? 'iPhone' : false);
    }

    /**
     * Check iPod or false
     *
     * @return bool|string
     */
    public static function isIPod() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'iPod')) ? 'iPod' : false);
    }

    /**
     * Check android device or false
     *
     * @return bool|string
     */
    public static function isAndroid() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'Android')) ? 'Android' : false);
    }

    /**
     * Check WebOS or false
     *
     * @return bool|string
     */
    public static function isWebOS() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'webOS')) ? 'webOS' : false);
    }

    /**
     * Check windows mobile device or false
     *
     * @return bool|string
     */
    public static function isWindowsPhone() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'Windows Phone')) ? 'windowsPhone' : false);
    }

    /**
     * Check desktop windows OS or false
     *
     * @return bool|string
     */
    public static function isWindowsOS() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'Windows NT')) ? 'windows' : false);
    }

    /**
     * Check desktop OS name or false
     *
     * @return bool|string
     */
    public function isDesktopOS() {
        $windows   = self::isWindowsOS();
        $linux     = self::isLinuxOS();
        $sunOS     = self::isSunOS();
        $freebsd   = self::isFreeBsdOS();
        $qnx       = self::isQnxOS();
        $searchBot = self::isSearchBot();

        return (($windows) ? $windows :
            (($linux) ? $linux :
                (($sunOS) ? $sunOS :
                    (($freebsd) ? $freebsd :
                        (($qnx) ? $qnx :
                            (($searchBot) ? $searchBot :
                                false
                            )
                        )
                    )
                )
            )
        );
    }

    /**
     * Check Linux OS or false
     *
     * @return bool|string
     */
    public static function isLinuxOS() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], '(Linux)|(X11)')) ? 'linux' : false);
    }

    /**
     * Check freeBSD OS or false
     *
     * @return bool|string
     */
    public static function isFreeBsdOS() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'OpenBSD')) ? 'freeBSD' : false);
    }

    /**
     * Check sunOS or false
     *
     * @return bool|string
     */
    public static function isSunOS() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'SunOS')) ? 'sunOS' : false);
    }

    /**
     * Check QNX OS or false
     *
     * @return bool|string
     */
    public static function isQnxOS() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'QNX')) ? 'qnx' : false);
    }

    /**
     * Check search bot or false
     *
     * @return bool|string
     */
    public static function isSearchBot() {
        return SearchBotDetect::searchBotDetectName();
    }

    /**
     * Check OS/2 or false
     *
     * @return bool|string
     */
    public static function isOS2() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'OS/2')) ? 'os2' : false);
    }
}

/**
 * Detect search bot
 * Class SearchBotDetect
 */
class SearchBotDetect {

    public static function isNuhk() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'nuhk')) ? 'nuhk' : false);
    }

    public static function isGoogleBot() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'Googlebot')) ? 'googleBot' : false);
    }

    public static function isYammyBot() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'Yammybot')) ? 'yammyBot' : false);
    }

    public static function isOpenBot() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'Openbot')) ? 'openBot' : false);
    }

    public static function isSlurp() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'Slurp')) ? 'slurp' : false);
    }

    public static function isMSNBot() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'MSNBot')) ? 'MSNBot' : false);
    }

    public static function isAskBot() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'Ask Jeeves/Teoma')) ? 'ask' : false);
    }

    public static function isArchiver() {
        return (((bool)stripos($_SERVER['HTTP_USER_AGENT'], 'ia_archiver')) ? 'iaArchiver' : false);
    }

    public static function searchBotDetectName() {

        $nuhk     = SearchBotDetect::isNuhk();
        $google   = SearchBotDetect::isGoogleBot();
        $yammy    = SearchBotDetect::isYammyBot();
        $openBot  = SearchBotDetect::isYammyBot();
        $slurp    = SearchBotDetect::isSlurp();
        $msnBot   = SearchBotDetect::isMSNBot();
        $ask      = SearchBotDetect::isAskBot();
        $archiver = SearchBotDetect::isArchiver();

        return (($nuhk) ? $nuhk :
            (($google) ? $google :
                (($yammy) ? $yammy :
                    (($openBot) ? $openBot :
                        (($slurp) ? $slurp :
                            (($msnBot) ? $msnBot :
                                (($ask) ? $ask :
                                    (($archiver) ? $archiver :
                                        false
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
    }

}

/**
 * Helper for info about server's resource usage
 *
 * Works only in UNIX systems
 *
 * Class GSystemHelper
 */
class GSystemHelper {

    /**
     * Data server's resource usage
     *
     * @var array
     */
    protected $allData = array();

    /**
     * Get cpu load info from mpstat
     *
     * @var bool
     */
    public $mpStat = true;

    /**
     * Default param for check information
     *
     * @var array
     */
    private $paramsDefault = array(
        'diskTotal',
        'diskFree',
        'diskPercent',
        'diskUsed',
        'memoryTotal',
        'memoryFree',
        'memoryUsed',
        'memoryPercent',
        'cpuLoad'
    );

    public static $instance = null;

    /**
     * Input params
     *
     * @var array
     */
    private $params = array();

    /**
     * Buffer
     *
     * @var array
     */
    protected $output = array();

    private function __construct() {

    }

    /**
     * Get data
     *
     * @return array
     */
    public function getAllData() {
        return $this->allData;
    }

    /**
     * Setter
     *
     * @param $property
     * @param $value
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            if (method_exists($this, 'set' . ucfirst($property))) {
                $this->{'set' . ucfirst($property)};
            }
        }

    }

    /**
     * Setter for params
     *
     * @param $params
     */
    private function setParams($params) {
        if (is_null($params)) {
            $this->params = $this->paramsDefault;

            return;
        }

        if (!is_array($params)) {
            $params = array($params);
        }

        $paramMerge = array();

        foreach ($this->paramsDefault as $_next) {
            if (in_array($_next, $params)) {
                $paramMerge[] = $_next;
            }
        }
        $this->params = $paramMerge;
    }

    public static function getInstance() {
        return (is_null(self::$instance) ? self::$instance = new self() : self::$instance);
    }

    /**
     * Get info about system's resource usage
     *
     * @param $params
     *
     * @return array
     */
    public function getInfo($params) {
        $this->setParams($params);

        if (sizeof($this->params)) {
            foreach ($this->params as $_paramNext) {
                $this->allData[$_paramNext] = $this->{'infoGet_' . $_paramNext}();
            }
        }

        return $this->allData;
    }

    /**
     * Info about disk usage
     */
    private function setOutputDisk() {
        if (!isset($this->output['disk'])) {
            $diskInfo = `df -h /`;
            preg_match_all('/[\S]+/', $diskInfo, $temp);
            $this->output['disk'] = $temp[0];
        }

    }

    /**
     * Total disk size
     *
     * @return mixed
     */
    private function infoGet_diskTotal() {
        $this->setOutputDisk();

        return $this->output['disk'][sizeof($this->output['disk']) - 4];
    }

    /**
     * Free disk space
     *
     * @return mixed
     */
    private function infoGet_diskFree() {
        $this->setOutputDisk();

        return $this->output['disk'][sizeof($this->output['disk']) - 2];
    }

    /**
     * Disk space used
     *
     * @return mixed
     */
    private function infoGet_diskUsed() {
        $this->setOutputDisk();

        return $this->output['disk'][sizeof($this->output['disk']) - 3];
    }

    /**
     * Disk usage in percent
     *
     * @return float|int
     */
    private function infoGet_diskPercent() {
        $this->setOutputDisk();

        return ($this->infoGet_diskUsed() > 0) ? round(floatval($this->infoGet_diskUsed()) /
                                                       floatval($this->infoGet_diskTotal() * 100)) : 0;
    }

    /**
     * CPU load info
     *
     * @return float|int
     */
    private function infoGet_cpuLoad() {

        if ($this->mpStat) {

            preg_match_all('/[\S]+/', `mpstat`, $loadCPU);

            return round(100 - $loadCPU[0][sizeof($loadCPU[0]) - 1], 1);

        }

        if (!function_exists('sys_getloadavg')) {
            return 1;
        }
        $loadCPU = sys_getloadavg();

        return round((($loadCPU[0] + $loadCPU[1] + $loadCPU[2]) / 3), 1) * 100;
    }

    /**
     * Get RAM info
     */
    private function memInfoOutput() {
        if (!isset($this->output['memory'])) {
            $memoryInfo = `cat /proc/meminfo`;
            preg_match_all('/[\S]+/', $memoryInfo, $this->output['memory']);
        }
    }

    /**
     * Total RAM space
     *
     * @return int
     */
    private function infoGet_memoryTotal() {
        $this->memInfoOutput();

        return floor(intval(@$this->output['memory'][0][1]) / 1024) ? : 1;
    }

    /**
     * Free RAM space
     *
     * @return float
     */
    private function infoGet_memoryFree() {
        $this->memInfoOutput();

        return floor(intval(@$this->output['memory'][0][10]) + intval($this->output['memory'][0][4]) / 1024);
    }

    /**
     * Usage RAM space
     *
     * @return int
     */
    private function infoGet_memoryUsed() {
        $this->memInfoOutput();

        return intval($this->infoGet_memoryTotal() - $this->infoGet_memoryFree()) ? : 1;
    }

    /**
     * Usage RAM space in percent
     *
     * @return int
     */
    private function infoGet_memoryPercent() {
        $this->memInfoOutput();

        return round((($this->infoGet_memoryFree() / $this->infoGet_memoryTotal()) * 100), 1) ? : 1;
    }
}