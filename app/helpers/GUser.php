<?php

class GUser {

    public static function isGuest() {
        return Yii::app()->user->isGuest;
    }

    public static function id() {
        if (self::isGuest()) {
            return false;
        }

        return Yii::app()->user->id;
    }

    public static function info() {
        if (self::isGuest()) {
            return false;
        }
        $userInfo = Users::model()
                         ->findByPk(self::id());
        if (is_null($userInfo)) {
            return false;
        }
        $userInfoLast = $userInfo->attributes;
        if (isset($userInfoLast['password'])) {
            unset($userInfoLast['password']);
        }

        if (isset($userInfoLast['token'])) {
            unset($userInfoLast['token']);
        }

        if (isset($userInfoLast['avatar'])) {
            $userInfoLast['avatar'] = GPath::baseUrl() . '/api/checkAvatar?url=' . $userInfoLast['avatar'];
        }

        return $userInfoLast;
    }

    public static function socName() {
        if (self::isGuest()) {
            return false;
        }
        $userInfo = self::info();
        if (GAnswer::isAjax()) {
            GAnswer::eTrue('', array(
                    'soc_name' => $userInfo['soc_name']
                )
            );
        }

        return $userInfo['soc_name'];
    }

    public static function getUserSoc_name() {
        $user = self::info();

        if (!is_null($user)) {
            return $user['soc_name'];
        }

        return false;
    }

}