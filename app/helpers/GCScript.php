<?php

class GCScript {
    public static function regScript($fileName, $position = CClientScript::POS_HEAD, $media = 'screen') {
        GScriptHelper::getInstance()
                     ->regFile($fileName, $position, $media);
    }
}

class GScriptHelper {

    protected static $instance;

    private $list     = array();
    private $media    = '';
    private $position = CClientScript::POS_HEAD;

    /** @var \CClientScript|mixed|null|void */
    private $cs = null;

    private function __construct() {
        $this->cs = Yii::app()
                       ->getClientScript();
    }

    final public static function getInstance() {
        return (self::$instance === null) ? self::$instance = new self() : self::$instance;
    }

    public function setList($list) {
        if (is_null($list)) {
            return false;
        }
        $this->list = is_array($list) ? $list : array($list);
    }

    public function setPosition($position) {
        if (!is_int($position)) {
            return false;
        }
        $this->position = $position;
    }

    public function setMedia($media) {
        if (!is_string($media)) {
            return false;
        }

        $this->media = $media;
    }

    private function path($file) {
        return strtolower(pathinfo($file, PATHINFO_EXTENSION));
    }

    private function scriptMap($file, $link) {
        $this->cs->scriptMap[$file . '.js'] = $link;
    }

    private function regCoreScript($file, $link = null) {
        $filename = pathinfo($file, PATHINFO_FILENAME); // From php 5.2.0
        if (!is_null($link)) {
            $this->scriptMap($filename, $link);
        }
        Yii::app()->clientScript->registerCoreScript($file);
    }

    private function regScriptFile($file, $position = CClientScript::POS_HEAD) {
        $this->cs->registerScriptFile($file, $position);
    }

    private function regScript($script, $id, $position = CClientScript::POS_END, $htmlOptions = array()) {
        $this->cs->registerScript($id, $script, $position);
    }

    private function regCssFile($file, $media = 'screen') {
        $this->cs->registerCssFile($file, $media);
    }

    private function regCss($script, $id, $media = null) {
        $this->cs->registerCss($id, $script, $media);
    }

    public function regFile($list = null, $position = null, $media = null) {
        $this->setList($list);
        $this->setPosition($position);
        $this->setMedia($media);

        foreach ($this->list as $_next) {
            if (is_array($_next)) {
                $this->nextArr($_next);
            } else {
                $this->nextFile($_next);
            }
        }
    }

    private function nextArr($arrFile) {
        $ext = $this->path($arrFile['name']);

        $position = (isset($arrFile['pos'])) ? $arrFile['pos'] : $this->position;
        $media    = isset($arrFile['media']) ? $arrFile['media'] : $this->media;

        $id   = isset($arrFile['id']) ? $arrFile['id'] : '';
        $html = isset($arrFile['opt']) ? $arrFile['opt'] : array();
        $link = isset($arrFile['link']) ? $arrFile['link'] : null;
        $type = isset($arrFile['type']) ? $arrFile['type'] : false;

        if (($ext == 'js') || $type == 'js') {
            $this->regScriptFile($arrFile['name'], $position);
        } else if ($ext == 'css' || $type == 'css') {
            $this->regCssFile($arrFile['name'], $media);
        } else {
            $this->scriptLink($arrFile['name'], $position, $id, $media, $html, $link);
            if (strpos($arrFile['name'], 'js: ') == 0) {
                $this->regScript($this->prepareScript($arrFile['name']), $id, $position, $html);
            } else {
                if (strpos($arrFile['name'], 'css: ') == 0) {
                    $this->regCss($this->prepareScript($arrFile['name']), $id, $media);
                } else {
                    $this->regCoreScript($arrFile['name'], $link);
                }
            }
        }
    }

    private function prepareScript($script) {
        return substr($script, 3, strlen($script));
    }

    private function nextFile($file) {
        $ext = $this->path($file);

        switch ($ext) {
            case 'js' :
                $this->regScriptFile($file, $this->position);
                break;
            case 'cssp' :
            case 'css' :
                $this->regCssFile($file, $this->media);
                break;
            default :
                $this->scriptLink($file, $this->position, '', $this->media);
                break;
        }
    }

    private function scriptLink($script, $position, $id = null, $media = null, $html = null, $link = null) {
        if (strpos($script, 'js: ') == 0 && !is_bool(strpos($script, 'js: '))) {
            $this->regScript($this->prepareScript($script), $id, $position, $html);
        } else {
            if (strpos($script, 'css: ') == 0 && !is_bool(strpos($script, 'js: '))) {
                $this->regCss($this->prepareScript($script), $id, $media);
            } else {
                $this->regCoreScript($script, $link);
            }
        }

    }

}