<?php

/**
 * Helper info path
 *
 * Class GPath
 */
class GPath {

    /**
     * Get Home URl
     *
     * @return mixed|void
     */
    public static function homeUrl() {
        return Yii::app()->homeUrl;
    }

    /**
     * Get Admin Media File URL
     *
     * @return string
     */
    public static function adminUrl() {
        return self::urlMedia() . '/admin';
    }

    /**
     * Media Folder URL
     *
     * @return string
     */
    public static function urlMedia() {
        return self::baseUrl() . '/shared/media';
    }

    /**
     * CSS Folder URL
     *
     * @return string
     */
    public static function urlCss() {
        return self::urlMedia() . '/css';
    }

    /**
     * Image flder URL
     *
     * @return string
     */
    public static function urlImage() {
        return self::urlMedia() . '/images';
    }

    /**
     * JS folder URL
     *
     * @return string
     */
    public static function urlJs() {
        return self::urlMedia() . '/js';
    }

    /**
     * Upload folder URL
     *
     * @return string
     */
    public static function urlUpload() {
        return self::baseUrl() . '/shared/uploads';
    }

    /**
     * Base URL
     *
     * @return mixed
     */
    public static function baseUrl() {
        return Yii::app()
                  ->createAbsoluteUrl('/');
    }

    /**
     * Bootstrap folder URL
     *
     * @param string $version
     *
     * @return string
     */
    public static function bootstrapUrl($version = '') {
        return self::adminUrl() . '/bootstrap' . $version;
    }

    /**
     * Check active link
     *
     * @param $component
     *
     * @return string
     */
    public static function activeLink($component) {
        $allWord = array();
        if (is_string($component)) {
            $allWord[] = $component;
        }

        if (is_array($component)) {
            $allWord = $component;
        }

        $return = '';

        foreach ($allWord as $_word) {
            if (strpos(Yii::app()->request->requestUri, $_word) !== false) {
                $return = ' active';
            }
        }

        return $return;
    }

    /**
     * Dynamic Path from $id
     *
     * @param $id
     *
     * @return string
     */
    public static function dynamicPath($id) {
        $n = (int)$id;

        $a = (int)($n / 30000);
        $b = (int)(($n - $a * 30000) / 300);

        return "{$a}/{$b}";
    }

    /**
     * Recursively create folder
     *
     * @param     $path
     * @param int $mode
     *
     * @return bool
     */
    public static function rmkdir($path, $mode = 0777) {
        return is_dir($path) || (self::rmkdir(dirname($path), $mode) && mkdir($path, $mode));
    }

    /**
     * Base Path
     *
     * @return mixed|void
     */
    public static function basePath() {
        return Yii::app()->basePath;
    }

    /**
     * Get Url
     *
     * @param CController $controller
     * @param             mixed {string|null} $addLink
     * @param bool        $absolute
     *
     * @return string
     */
    public static function getURL($controller, $addLink = null, $absolute = true) {
        $module = '';
        if (isset($controller->module) && !empty($controller->module->id)) {
            $module = $controller->module->id . "/" . ((is_string($addLink)) ? $addLink : '');
        }

        return Yii::app()
                  ->getBaseUrl($absolute) . self::trimSlashes($module);
    }

    /**
     * Split url vs /
     *
     * @param string $url
     *
     * @return mixed|string
     */
    public static function trimSlashes($url) {
        $url = str_replace("//", "/", $url);

        if ($url[0] != '/') {
            $url = "/" . $url;
        }

        return $url;
    }

    /**
     * Get assets manager URL
     *
     * @param null|CWebModule $module
     *
     * @return string
     */
    public static function assetsUrl($module=null) {
        if ($module instanceof CWebModule) {
            if (property_exists($module, 'assetsUrl'))
                return $module->assetsUrl;
            if (method_exists($module, 'getAssetsUrl'))
                return $module->getAssetsUrl();
        }
        return Yii::app()->getAssetManager()->baseUrl;
    }

    /**
     * Create URL
     *
     * @param string $route
     * @param Controller|null $controller
     *
     * @return string
     */
    public static function createUrl($route, $controller=null) {
        if ($controller instanceof Controller || $controller instanceof CController) {
            return $controller->createUrl($route);
        }

        return self::baseUrl() . self::trimSlashes($route);
    }

    /**
     * Get link for social network login
     *
     * @param $socName
     *
     * @return string
     */
    public static function socUrl($socName, $htmlStyle=false) {
        switch ($socName) {
            case 'vk' :
                $url = Yii::app()->vk->LoginUrl();
                break;
            case 'ok' :
                $url = Yii::app()->ok->loginUrl();
                break;
            case 'fb' :
                $url = Yii::app()->fb->loginUrl();
                break;
            default :
                $url = '';
                break;
        }

        if ($htmlStyle) {
            $url = str_replace('&', '&amp;', $url);
        }

        return $url;

    }

}
