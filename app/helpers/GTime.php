<?php

class GTime {

    public static function timeNow($format = 'Y-m-d H:i:s') {
        return date($format, time());
    }

    public static function convertTime($time = null, $format = 'Y-m-d H:i:s') {
        return date($format, ((!is_null($time)) ? $time : time()));
    }

}