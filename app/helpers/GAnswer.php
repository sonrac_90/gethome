<?php

class GAnswer {

    public static $errorField = 'errorTxt';

    public static function isAjax() {
        return Yii::app()->request->isAjaxRequest;
    }

    public static function eFalse($message, $param = array()) {
        self::eResult(false, $message, $param);
    }

    public static function eTrue($message, $param = array()) {
        $param = (!is_array($param) ? array() : $param);
        self::eResult(true, $message, $param);
    }

    public static function eResult($result, $message, $param) {
        echo self::answer(
            array_merge(
                array('status' => $result),
                array(self::$errorField => $message),
                $param
            )
        );
        Yii::app()
           ->end();
    }

    public static function jFalse($message, $param = array()) {
        return self::jResult(false, $message, $param);
    }

    public static function jTrue($message, $param = array()) {
        return self::jResult(true, $message, $param);
    }

    public static function jResult($result, $message, $param) {
        return self::answer(
            array_merge(
                array('status' => $result),
                array(self::$errorField => $message),
                $param
            )
        );
    }

    public static function answer($param) {
        if (!is_array($param)) {
            $param = array($param);
        }

        return self::encode($param);
    }

    public static function encode($arr) {
        return CJSON::encode($arr);
    }

    public static function decode($arr, $toArray = true) {
        return CJSON::decode($arr, $toArray);
    }

}