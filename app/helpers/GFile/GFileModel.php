<?php

/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 09.12.13
 * Time: 20:34
 */
class GFileModel extends CFormModel {

    public $image;
    public $video;

    public function rules() {
        return array(
            array(
                'image',
                'file',
                'types' => 'jpg, gif, png',
                'allowEmpty' => true,
                'on' => 'image'
            ),
            array(
                'video',
                'file',
                'types' => 'avi, mp4, m4v, mkv',
                'allowEmpty' => true,
                'on' => 'video'
            ),
        );
    }

} 